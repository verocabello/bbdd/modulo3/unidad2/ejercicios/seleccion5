<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Ciclista;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use app\models\Lleva;
use app\models\Puerto;
use app\models\Etapa;
use app\models\Equipo;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCrud() {
        return $this->render("gestion");
    }
      
    public function actionConsulta1a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select("nombre,edad")
                ->distinct()
                ->joinWith('etapas',false,'left join')
                ->where('isNull(etapa.dorsal)'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','edad'],
            "titulo"=> "Consulta 1 con Active Record",
            "enunciado"=>"Nombre y edad de los ciclistas que han ganado etapas",
            "sql"=>"SELECT DISTINCT nombre,edad FROM ciclista LEFT JOIN etapa ON ciclista.dorsal=etapa.dorsal WHERE etapa.dorsal IS NULL",
        ]);
    }
    
    public function actionConsulta1() {
        // mediante DAO
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT DISTINCT nombre,edad FROM ciclista LEFT JOIN etapa ON ciclista.dorsal=etapa.dorsal WHERE etapa.dorsal IS NULL',
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','edad'],
            "titulo"=> "Consulta 1 con DAO",
            "enunciado"=>"Nombre y edad de los ciclistas que han ganado etapas",
            "sql"=>"SELECT DISTINCT nombre,edad FROM ciclista LEFT JOIN etapa ON ciclista.dorsal=etapa.dorsal WHERE etapa.dorsal IS NULL",
        ]);
    }
    
    public function actionConsulta2a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select("nombre,edad")
                ->distinct()
                ->joinWith('puertos',false,'left join')
                ->where('isNull(puerto.dorsal)'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','edad'],
            "titulo"=> "Consulta 2 con Active Record",
            "enunciado"=>"Nombre y edad de los ciclistas que han ganado puertos",
            "sql"=>"SELECT DISTINCT nombre,edad FROM ciclista LEFT JOIN puerto ON ciclista.dorsal=puerto.dorsal WHERE puerto.dorsal IS NULL",
        ]);
    }
    
    public function actionConsulta2() {
        // mediante DAO
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT DISTINCT nombre,edad FROM ciclista LEFT JOIN puerto ON ciclista.dorsal=puerto.dorsal WHERE puerto.dorsal IS NULL',
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','edad'],
            "titulo"=> "Consulta 2 con DAO",
            "enunciado"=>"Nombre y edad de los ciclistas que han ganado puertos",
            "sql"=>"SELECT DISTINCT nombre,edad FROM ciclista LEFT JOIN puerto ON ciclista.dorsal=puerto.dorsal WHERE puerto.dorsal IS NULL",
        ]);
    }
    
    public function actionConsulta3a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Equipo::find()
                ->select("director")
                ->distinct()
                ->joinWith('ciclistas',false,'inner join')
                ->leftJoin('etapa','ciclista.dorsal=etapa.dorsal')
                ->where('isNull(etapa.dorsal)'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['director'],
            "titulo"=> "Consulta 3 con Active Record",
            "enunciado"=>"Listar el director de los equipos que tengan ciclistas no que hayan ganado ninguna etapa",
            "sql"=>"SELECT DISTINCT director FROM equipo JOIN ciclista USING(nomequipo) LEFT JOIN etapa ON ciclista.dorsal=etapa.dorsal WHERE etapa.dorsal IS NULL",
        ]);
    }
    
    public function actionConsulta3() {
        // mediante DAO
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT DISTINCT director FROM equipo JOIN ciclista USING(nomequipo) LEFT JOIN etapa ON ciclista.dorsal=etapa.dorsal WHERE etapa.dorsal IS NULL',
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['director'],
            "titulo"=> "Consulta 3 con DAO",
            "enunciado"=>"Listar el director de los equipos que tengan ciclistas no que hayan ganado ninguna etapa",
            "sql"=>"SELECT DISTINCT director FROM equipo JOIN ciclista USING(nomequipo) LEFT JOIN etapa ON ciclista.dorsal=etapa.dorsal WHERE etapa.dorsal IS NULL",
        ]);
    }
    
    public function actionConsulta4a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select("ciclista.dorsal,ciclista.nombre")
                ->distinct()
                ->leftJoin('lleva','ciclista.dorsal=lleva.dorsal')
                ->where('isNull(lleva.dorsal)'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','nombre'],
            "titulo"=> "Consulta 4 con Active Record",
            "enunciado"=>"Dorsal y nombre de los ciclistas que no hayan llevado algun maillot",
            "sql"=>"SELECT DISTINCT ciclista.dorsal,ciclista.nombre FROM ciclista LEFT JOIN lleva ON ciclista.dorsal=lleva.dorsal WHERE lleva.dorsal IS NULL",
        ]);
    }
    
    public function actionConsulta4() {
        // mediante DAO
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT DISTINCT ciclista.dorsal,ciclista.nombre FROM ciclista LEFT JOIN lleva ON ciclista.dorsal=lleva.dorsal WHERE lleva.dorsal IS NULL',
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','nombre'],
            "titulo"=> "Consulta 4 con DAO",
            "enunciado"=>"Dorsal y nombre de los ciclistas que no hayan llevado algun maillot",
            "sql"=>"SELECT DISTINCT ciclista.dorsal,ciclista.nombre FROM ciclista LEFT JOIN lleva ON ciclista.dorsal=lleva.dorsal WHERE lleva.dorsal IS NULL",
        ]);
    }
    
    public function actionConsulta5a() {
        // mediante active record
        
        $subconsulta=Lleva::find()
                ->select("lleva.dorsal")
                ->distinct()
                ->innerJoin('maillot','lleva.código=maillot.código')
                ->where("color='amarillo'");
        
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select("ciclista.dorsal,ciclista.nombre")
                ->distinct()
                ->leftJoin(['c1'=>$subconsulta],'ciclista.dorsal=c1.dorsal')
                ->where('isNull(c1.dorsal)'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','nombre'],
            "titulo"=> "Consulta 5 con Active Record",
            "enunciado"=>"Dorsal y nombre de los ciclistas que no hayan llevado el maillot amarillo nunca",
            "sql"=>"SELECT ciclista.dorsal,ciclista.nombre FROM ciclista LEFT JOIN (
                        SELECT DISTINCT dorsal FROM lleva JOIN maillot USING(código) WHERE color='amarillo') c1
                            ON ciclista.dorsal=c1.dorsal WHERE c1.dorsal IS NULL",
        ]);
    }
    
    public function actionConsulta5() {
        // mediante DAO
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT ciclista.dorsal,ciclista.nombre FROM ciclista LEFT JOIN (
                        SELECT DISTINCT dorsal FROM lleva JOIN maillot USING(código) WHERE color="amarillo") c1
                            ON ciclista.dorsal=c1.dorsal WHERE c1.dorsal IS NULL',
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','nombre'],
            "titulo"=> "Consulta 5 con DAO",
            "enunciado"=>"Dorsal y nombre de los ciclistas que no hayan llevado el maillot amarillo nunca",
            "sql"=>"SELECT ciclista.dorsal,ciclista.nombre FROM ciclista LEFT JOIN (
                        SELECT DISTINCT dorsal FROM lleva JOIN maillot USING(código) WHERE color='amarillo') c1
                            ON ciclista.dorsal=c1.dorsal WHERE c1.dorsal IS NULL",
        ]);
    }
    
    public function actionConsulta6a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Etapa::find()
                ->select("etapa.numetapa")
                ->distinct()
                ->leftJoin('puerto','etapa.numetapa=puerto.numetapa')
                ->where('isNull(puerto.numetapa)'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['numetapa'],
            "titulo"=> "Consulta 6 con Active Record",
            "enunciado"=>"Indicar el numetapa de las etapas que no tengan puertos",
            "sql"=>"SELECT DISTINCT etapa.numetapa FROM etapa LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS NULL",
        ]);
    }
    
    public function actionConsulta6() {
        // mediante DAO
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT DISTINCT etapa.numetapa FROM etapa LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS NULL',
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['numetapa'],
            "titulo"=> "Consulta 6 con DAO",
            "enunciado"=>"Indicar el numetapa de las etapas que no tengan puertos",
            "sql"=>"SELECT DISTINCT etapa.numetapa FROM etapa LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS NULL",
        ]);
    }
    
    public function actionConsulta7a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Etapa::find()
                ->select("AVG(etapa.kms) total")
                ->distinct()
                ->leftJoin('puerto','etapa.numetapa=puerto.numetapa')
                ->where('isNull(puerto.numetapa)'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=> "Consulta 7 con Active Record",
            "enunciado"=>"Indicar la distancia media de las etapas que no tengan puertos",
            "sql"=>"SELECT AVG(etapa.kms) FROM etapa LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS NULL",
        ]);
    }
    
    public function actionConsulta7() {
        // mediante DAO
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT AVG(etapa.kms) numero FROM etapa LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS NULL',
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['numero'],
            "titulo"=> "Consulta 7 con DAO",
            "enunciado"=>"Indicar la distancia media de las etapas que no tengan puertos",
            "sql"=>"SELECT AVG(etapa.kms) FROM etapa LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS NULL",
        ]);
    }
    
    public function actionConsulta8a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select("COUNT(ciclista.dorsal) total")
                ->distinct()
                ->JoinWith('etapas',false,'left join')
                ->where('isNull(etapa.dorsal)'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=> "Consulta 8 con Active Record",
            "enunciado"=>"Listar el numero de ciclistas que no hayan ganado alguna etapa",
            "sql"=>"SELECT COUNT(ciclista.dorsal) FROM ciclista LEFT JOIN etapa ON ciclista.dorsal=etapa.dorsal WHERE etapa.dorsal IS NULL",
        ]);
    }
    
    public function actionConsulta8() {
        // mediante DAO
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT COUNT(ciclista.dorsal) numero FROM ciclista LEFT JOIN etapa ON ciclista.dorsal=etapa.dorsal WHERE etapa.dorsal IS NULL',
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['numero'],
            "titulo"=> "Consulta 8 con DAO",
            "enunciado"=>"Listar el numero de ciclistas que no hayan ganado alguna etapa",
            "sql"=>"SELECT COUNT(ciclista.dorsal) FROM ciclista LEFT JOIN etapa ON ciclista.dorsal=etapa.dorsal WHERE etapa.dorsal IS NULL",
        ]);
    }
    
    public function actionConsulta9a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select("ciclista.dorsal")
                ->distinct()
                ->JoinWith('etapas',false,'inner join')
                ->leftJoin('puerto','etapa.numetapa=puerto.numetapa')
                ->where('isNull(puerto.numetapa)'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=> "Consulta 9 con Active Record",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado alguna etapa que no tenga puerto",
            "sql"=>"SELECT DISTINCT ciclista.dorsal FROM ciclista JOIN etapa USING(dorsal) LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS NULL",
        ]);
    }
    
    public function actionConsulta9() {
        // mediante DAO
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT DISTINCT ciclista.dorsal FROM ciclista JOIN etapa USING(dorsal) LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS NULL',
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=> "Consulta 9 con DAO",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado alguna etapa que no tenga puerto",
            "sql"=>"SELECT DISTINCT ciclista.dorsal FROM ciclista JOIN etapa USING(dorsal) LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS NULL",
        ]);
    }
    
    public function actionConsulta10a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select("ciclista.dorsal")
                ->distinct()
                ->JoinWith('etapas',false,'inner join')
                ->leftJoin('puerto','etapa.numetapa=puerto.numetapa')
                ->where('isNull(puerto.numetapa)'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=> "Consulta 10 con Active Record",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado unicamente etapas que no tengan puertos",
            "sql"=>"SELECT DISTINCT ciclista.dorsal FROM ciclista JOIN etapa USING(dorsal) LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS NULL",
        ]);
    }
    
    public function actionConsulta10() {
        // mediante DAO
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT DISTINCT ciclista.dorsal FROM ciclista JOIN etapa USING(dorsal) LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS NULL',
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=> "Consulta 10 con DAO",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado unicamente etapas que no tengan puertos",
            "sql"=>"SELECT DISTINCT ciclista.dorsal FROM ciclista JOIN etapa USING(dorsal) LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS NULL",
        ]);
    }
        
}